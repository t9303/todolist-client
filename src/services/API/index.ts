import { IAPI, ITodo, ITodoInput } from 'models';
import APIFactory from './factory';

export default class APIHandler {
	private strategy: IAPI;
	private static handlerInstance: APIHandler;

	private constructor(strategy: IAPI) {
		this.strategy = strategy;
	}

	public static getInstance = (): APIHandler => {
		if (!APIHandler.handlerInstance) {
			APIHandler.handlerInstance = new APIHandler(
				APIFactory(process.env.API_METHOD)
			);
		}

		return APIHandler.handlerInstance;
	};

	public create = (todo: ITodoInput): Promise<ITodo> => {
		return this.strategy.create(todo);
	};

	public update = (todo: ITodo): Promise<boolean> => {
		return this.strategy.update(todo);
	};

	public delete = (todoID: string): Promise<boolean> => {
		return this.strategy.delete(todoID);
	};

	public getAll = (): Promise<ITodo[]> => {
		return this.strategy.getAll();
	};
}
