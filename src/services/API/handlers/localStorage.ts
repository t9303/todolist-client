import { v1 } from 'uuid';
import { IAPI, ITodo, ITodoInput } from 'models';

const okResponse: Response = new Response('', {
	status: 200
});
const todoListKey: string = 'todoList';
const updateList = (updatedList: ITodo[]): void => {
	localStorage.setItem(todoListKey, JSON.stringify(updatedList));
};

export default class LocalStorageHandler implements IAPI {
	public create = async (todo: ITodoInput): Promise<ITodo> => {
		const updatedTodo: ITodo = {
			...todo,
			id: v1()
		};
		const updatedList: ITodo[] = [...(await this.getAll()), updatedTodo];

		updateList(updatedList);

		return updatedTodo;
	};

	public update = async (newTodo: ITodo): Promise<boolean> => {
		const updatedList: ITodo[] = (await this.getAll()).map((todo: ITodo) =>
			todo.id === newTodo.id ? newTodo : todo
		);

		updateList(updatedList);

		return okResponse.ok;
	};

	public delete = async (todoID: string): Promise<boolean> => {
		const currentList: ITodo[] = await this.getAll();
		const updatedList: ITodo[] = currentList.filter(
			(todo: ITodo) => todo.id !== todoID
		);

		if (currentList.length !== updatedList.length) {
			updateList(updatedList);
			return okResponse.ok;
		} else {
			throw new Error("The Todo you're trying to delete wasn't found. oops :(");
		}
	};

	public getById = async (todoID: string): Promise<ITodo> => {
		const todo: ITodo | undefined = (await this.getAll()).find(
			(currentTodo: ITodo) => currentTodo.id === todoID
		);

		if (!todo) {
			throw new Error("Your todo couldn't be found");
		} else {
			return todo;
		}
	};

	public getAll = async (): Promise<ITodo[]> => {
		const list: string | null = localStorage.getItem(todoListKey);

		if (!list) {
			const newList: ITodo[] = [];
			updateList(newList);
			return newList;
		}

		return JSON.parse(list) as ITodo[];
	};
}
