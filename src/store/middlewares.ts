import { useRecoilState, useSetRecoilState } from 'recoil';
import { ITodo, ITodoInput, Status } from '@/models';
import APIHandler from '@/services/API';
import { todoList, statusSearch, textSearch } from './atoms';

type TodolistInitHook = () => void;
type TodolistAddHook = (todo: ITodoInput) => void;
type TodolistUpdateHook = (todo: ITodo) => void;
type TodolistDeleteHook = (todoId: string) => void;
type TextSearchUpdateHook = (text: string) => void;
type StatusSearchUpdateHook = (status: Status) => void;

const storageHandler = APIHandler.getInstance();

export const useLoadTodos = (): TodolistInitHook => {
	const setList = useSetRecoilState<ITodo[]>(todoList);

	return () => {
		storageHandler.getAll().then(setList);
	};
};

export const useAddTodo = (): TodolistAddHook => {
	const [list, setList] = useRecoilState<ITodo[]>(todoList);

	return (newTodo: ITodoInput): void => {
		storageHandler.create(newTodo).then((updatedNewTodo: ITodo) => {
			setList([...list, updatedNewTodo]);
		});
	};
};

export const useUpdateTodo = (): TodolistUpdateHook => {
	const [list, setList] = useRecoilState<ITodo[]>(todoList);

	return (todo: ITodo): void => {
		storageHandler.update(todo).then(() => {
			setList(list.map((item: ITodo) => (item.id === todo.id ? todo : item)));
		});
	};
};

export const useDeleteTodo = (): TodolistDeleteHook => {
	const [list, setList] = useRecoilState<ITodo[]>(todoList);

	return (todoId: string): void => {
		storageHandler.delete(todoId).then(() => {
			setList(list.filter((item: ITodo) => item.id !== todoId));
		});
	};
};

export const useUpdateStatusSearch = (): StatusSearchUpdateHook => {
	const setSelectedStatus = useSetRecoilState<Status>(statusSearch);

	return (newStatus: Status): void => {
		setSelectedStatus(newStatus);
	};
};

export const useUpdateTextSearch = (): TextSearchUpdateHook => {
	const setText = useSetRecoilState<string>(textSearch);

	return (newTextSearch: string): void => {
		setText(newTextSearch);
	};
};
