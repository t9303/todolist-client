import { useRef, useEffect, useMemo, MutableRefObject } from 'react';

export const useMemoizedEffect = (
	callback: (...deps: any[]) => void,
	outerScopeDeps: any[]
) => {
	const memoizedDeps: MutableRefObject<any>[] = outerScopeDeps.map(useRef);

	const memoizedEffect = useMemo(
		() => () => {
			callback(
				...memoizedDeps.map((param: MutableRefObject<any>) => param.current)
			);
		},
		[callback, memoizedDeps]
	);

	useEffect(memoizedEffect, [memoizedEffect]);
};
