import React from 'react';
import CreateBar from './CreateBar';
import SearchBar from './SearchBar';
import './actions-bar.scss';

const ActionsBar: React.FC = () => {
	return (
		<div className='actions-bar'>
			<CreateBar />
			<SearchBar />
		</div>
	);
};

export default ActionsBar;
