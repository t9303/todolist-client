import React, { useState, useCallback } from 'react';
import { ITodo } from 'models';
import { useUpdateTodo } from 'store';
import EditNameMode from './EditNameMode';

interface Props {
	todo: ITodo;
}

const TodoNameContainer: React.FC<Props> = ({ todo }: Props) => {
	const updateHandler = useUpdateTodo();
	const [isEditing, setEditMode] = useState<boolean>(false);

	const submit = useCallback(
		(todoName: string): void => {
			updateHandler({ ...todo, title: todoName });
		},
		[todo, updateHandler]
	);

	const openEditMode = (): void => {
		setEditMode(true);
	};

	const closeEditMode = (): void => {
		setEditMode(false);
	};

	return isEditing ? (
		<EditNameMode
			submit={submit}
			placeholder={todo.title}
			closeEditMode={closeEditMode}
		/>
	) : (
		<span
			title='Click once to unwrap, Double click to edit name'
			onDoubleClick={openEditMode}
		>
			{todo.title}
		</span>
	);
};

export default TodoNameContainer;
