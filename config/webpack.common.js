'use-strict';

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const KB = 1024;
const srcPath = path.resolve('src');

module.exports = {
	entry: path.join(srcPath, 'index.tsx'),
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'ts-loader',
						options: {
							transpileOnly: true
						}
					}
				]
			},
			{
				test: /\.(s?c)ss$/i,
				use: ['style-loader', 'css-loader', 'sass-loader']
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				exclude: /node_modules/,
				type: 'asset',
				parser: {
					dataUrlCondition: {
						maxSize: 4 * KB
					}
				}
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			hash: true,
			inject: true,
			template: path.join(srcPath, 'index.html'),
			minify: {
				removeComments: true,
				collapseWhitespace: true,
				removeRedundantAttributes: true,
				useShortDoctype: true,
				removeEmptyAttributes: true,
				removeStyleLinkTypeAttributes: true,
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true
			}
		})
	],
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
		modules: ['node_modules', srcPath],
		alias: {
			'@': srcPath
		}
	}
};
